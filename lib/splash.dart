import 'package:anveshiapm/blocs/user.bloc.dart';
import 'package:anveshiapm/events/user.events.dart';
import 'package:anveshiapm/screens/homescreen.dart';
import 'package:anveshiapm/screens/login.dart';
import 'package:anveshiapm/shared.repo.dart';
import 'package:anveshiapm/states/user.state.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  final sharedRepo = SharedRepo();

  @override
  void initState() {
    print('Splash init');
    BlocProvider.of<UserBloc>(context).add(AuthenticateUser());
    super.initState();
  }

  @override
  void dispose() {
    print('Splash dispose');
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocConsumer<UserBloc, UserState>(
        builder: (context, state) {
          return Container(
            child: Image.asset(
              'assets/images/menu_bg.webp',
              fit: BoxFit.cover,
            ),
            width: double.infinity,
            height: double.infinity,
          );
        },
        listener: (context, state) {
          print('Splash Event Trigger');
          if (state is ValidUser) {
            Future.delayed(const Duration(seconds: 5), () {
              Navigator.of(context).pushReplacement(MaterialPageRoute(
                  builder: (BuildContext context) => HomeScreen()));
            });
          } else if (state is UserNotLoggedIn) {
            Future.delayed(const Duration(seconds: 5), () {
              Navigator.of(context).pushReplacement(MaterialPageRoute(
                  builder: (BuildContext context) => LoginScreen()));
            });
          }
        },
      ),
    );
  }
}
