class Media {
  String xsmall;
  int xsmallWidth;
  int xsmallHeight;
  String small;
  int smallWidth;
  int smallHeight;
  String medium;
  int mediumWidth;
  int mediumHeight;
  String thumb;
  int thumbWidth;
  int thumbHeight;
  String cover;
  int coverWidth;
  int coverHeight;
  String url;
  String urlNaked;

  Media(
      {this.xsmall,
      this.xsmallWidth,
      this.xsmallHeight,
      this.small,
      this.smallWidth,
      this.smallHeight,
      this.medium,
      this.mediumWidth,
      this.mediumHeight,
      this.thumb,
      this.thumbWidth,
      this.thumbHeight,
      this.cover,
      this.coverWidth,
      this.coverHeight,
      this.url,
      this.urlNaked});

  Media.fromJson(Map<String, dynamic> json) {
    xsmall = json['xsmall'];
    xsmallWidth = json['xsmall_width'];
    xsmallHeight = json['xsmall_height'];
    small = json['small'];
    smallWidth = json['small_width'];
    smallHeight = json['small_height'];
    medium = json['medium'];
    mediumWidth = json['medium_width'];
    mediumHeight = json['medium_height'];
    thumb = json['thumb'];
    thumbWidth = json['thumb_width'];
    thumbHeight = json['thumb_height'];
    cover = json['cover'];
    coverWidth = json['cover_width'];
    coverHeight = json['cover_height'];
    url = json['url'];
    urlNaked = json['url_naked'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['xsmall'] = this.xsmall;
    data['xsmall_width'] = this.xsmallWidth;
    data['xsmall_height'] = this.xsmallHeight;
    data['small'] = this.small;
    data['small_width'] = this.smallWidth;
    data['small_height'] = this.smallHeight;
    data['medium'] = this.medium;
    data['medium_width'] = this.mediumWidth;
    data['medium_height'] = this.mediumHeight;
    data['thumb'] = this.thumb;
    data['thumb_width'] = this.thumbWidth;
    data['thumb_height'] = this.thumbHeight;
    data['cover'] = this.cover;
    data['cover_width'] = this.coverWidth;
    data['cover_height'] = this.coverHeight;
    data['url'] = this.url;
    data['url_naked'] = this.urlNaked;
    return data;
  }
}
