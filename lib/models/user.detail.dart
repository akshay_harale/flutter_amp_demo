import '../models/list.dart';
import 'package:json_annotation/json_annotation.dart';

import 'artist.config.dart';

@JsonSerializable()
class Data {
  Artistconfig artistconfig;
  List<MediaInfo> list;
  String token;
  MetaIds metaids;
  CustomerData customer;

  Data({this.artistconfig, this.token, this.metaids, this.customer});

  Data.fromJson(Map<String, dynamic> json) {
    artistconfig = json['artistconfig'] != null
        ? new Artistconfig.fromJson(json['artistconfig'])
        : null;
    customer = json['customer'] != null
        ? new CustomerData.fromJson(json['customer'])
        : null;
    if (json['list'] != null) {
      list = new List<MediaInfo>();
      json['list'].forEach((v) {
        list.add(new MediaInfo.fromJson(v));
      });
    }
    token = json['token'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.artistconfig != null) {
      data['artistconfig'] = this.artistconfig.toJson();
    }
    if (this.list != null) {
      data['list'] = this.list.map((v) => v.toJson()).toList();
    }

    return data;
  }

  bool get isUserLogin {
    return token != null && token.isNotEmpty;
  }
}

class MetaIds {}

class CustomerData {
  String id;
  String firstName;
  String lastName;
  String picture;

  CustomerData({this.id, this.firstName, this.lastName, this.picture});

  CustomerData.fromJson(Map<String, dynamic> json) {
    id = json['_id'];
    firstName = json['first_name'];
    lastName = json['last_name'];
    picture = json['picture'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['_id'] = this.id;
    data['first_name'] = this.firstName;
    data['last_name'] = this.lastName;
    data['picture'] = this.picture;
    return data;
  }
}
