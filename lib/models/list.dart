import '../models/media.dart';

class MediaInfo {
  String id;
  String artistId;
  int level;
  String code;
  int ordering;
  String coins;
  String status;
  String mobileTabbarMenu;
  bool contentMenu;
  bool dashboardMenu;
  String webviewUrl;
  String languageId;
  String name;
  String caption;
  String slug;
  List<String> visiblity;
  List<String> platforms;
  String type;
  String updatedAt;
  String createdAt;
  String bucketId;
  List<String> contentTypes;
  Media photo;
  Media video;

  MediaInfo(
      {this.id,
      this.artistId,
      this.level,
      this.code,
      this.ordering,
      this.coins,
      this.status,
      this.mobileTabbarMenu,
      this.contentMenu,
      this.dashboardMenu,
      this.webviewUrl,
      this.languageId,
      this.name,
      this.caption,
      this.slug,
      this.visiblity,
      this.platforms,
      this.type,
      this.updatedAt,
      this.createdAt,
      this.bucketId,
      this.contentTypes,
      this.photo,
      this.video});

  MediaInfo.fromJson(Map<String, dynamic> json) {
    id = json['_id'];
    artistId = json['artist_id'];
    level = json['level'];
    code = json['code'];
    ordering = json['ordering'];
    coins = json['coins'];
    status = json['status'];
    mobileTabbarMenu = json['mobile_tabbar_menu'];
    contentMenu = json['content_menu'];
    dashboardMenu = json['dashboard_menu'];
    webviewUrl = json['webview_url'];

    languageId = json['language_id'];
    name = json['name'];
    caption = json['caption'];
    slug = json['slug'];

    if (json['visiblity'] != null) {
      visiblity = json['visiblity'].cast<String>();
    }

    if (json['platforms'] != null) {
      platforms = json['platforms'].cast<String>();
    }

    type = json['type'];

    updatedAt = json['updated_at'];
    createdAt = json['created_at'];
    bucketId = json['bucket_id'];

    if (json['content_types'] != null) {
      contentTypes = json['content_types'].cast<String>();
    }

    photo = json['photo'] != null ? new Media.fromJson(json['photo']) : null;
    video = json['video'] != null ? new Media.fromJson(json['video']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['_id'] = this.id;
    data['artist_id'] = this.artistId;
    data['level'] = this.level;
    data['code'] = this.code;
    data['ordering'] = this.ordering;
    data['coins'] = this.coins;
    data['status'] = this.status;
    data['mobile_tabbar_menu'] = this.mobileTabbarMenu;
    data['content_menu'] = this.contentMenu;
    data['dashboard_menu'] = this.dashboardMenu;
    data['webview_url'] = this.webviewUrl;

    data['language_id'] = this.languageId;
    data['name'] = this.name;
    data['caption'] = this.caption;
    data['slug'] = this.slug;
    data['visiblity'] = this.visiblity;
    data['platforms'] = this.platforms;
    data['type'] = this.type;

    data['updated_at'] = this.updatedAt;
    data['created_at'] = this.createdAt;
    data['bucket_id'] = this.bucketId;
    data['content_types'] = this.contentTypes;
    if (this.photo != null) {
      data['photo'] = this.photo.toJson();
    }
    if (this.video != null) {
      data['video'] = this.video.toJson();
    }
    return data;
  }
}
