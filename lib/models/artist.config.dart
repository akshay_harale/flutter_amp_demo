import '../models/artist.dart';
import '../models/media.dart';

class Artistconfig {
  String sId;
  String artistId;
  String channelNamespace;
  String fmcDefaultTopicId;
  String fmcDefaultTopicIdTest;
  String androidVersionName;
  int androidVersionNo;
  String iosVersionName;
  int iosVersionNo;
  String lastUpdatedGifts;
  String lastUpdatedBuckets;
  String lastUpdatedPackages;
  String lastVisited;
  String updatedAt;
  String createdAt;
  String s18PlusAgeContentAndroid;
  String s18PlusAgeContentIos;
  String androidAppDownloadLink;
  String directAppDownloadLink;
  String domain;
  String fbAccessToken;
  String fbApiKey;
  String fbApiSecret;
  String fbPageId;
  String fbPageName;
  String fbPageUrl;
  String fbUserName;
  String fcmServerKey;
  String instagramAccessToken;
  String instagramPageUrl;
  String instagramPassword;
  String instagramUserId;
  String instagramUserName;
  String iosAppDownloadLink;
  String key;
  int lastAssignedCommentsChannelno;
  String promotionalBanners;
  String pubnubPublishKey;
  String pubnubSubcribeKey;
  String sessionTimeout;
  String socialBucketId;
  String socketUrl;
  String twitterConsumerKey;
  String twitterConsumerKeySecret;
  String twitterOauthAccessToken;
  String twitterOauthAccessTokenSecret;
  String twitterPageUrl;
  String twitterUserName;
  String androidForceUpdate;
  String iosForceUpdate;
  String agoraId;

  String shoutoutHowToVideoStatus;

  int defaultAgeRestriction;
  Media logo;
  bool holidayMode;

  String lastUpdatedGenres;
  String androidUpdateMessage;
  String iosUpdateMessage;
  int minimumAndroidVersionNo;
  int minimumIosVersionNo;

  String removePrivateVideoCallIntroVideo;

  String removeShoutoutPoweredByPhoto;

  List<String> giftChannelName;
  List<String> commentChannelName;
  Artist artist;

  Artistconfig(
      {this.sId,
      this.artistId,
      this.channelNamespace,
      this.fmcDefaultTopicId,
      this.fmcDefaultTopicIdTest,
      this.androidVersionName,
      this.androidVersionNo,
      this.iosVersionName,
      this.iosVersionNo,
      this.lastUpdatedGifts,
      this.lastUpdatedBuckets,
      this.lastUpdatedPackages,
      this.lastVisited,
      this.updatedAt,
      this.createdAt,
      this.s18PlusAgeContentAndroid,
      this.s18PlusAgeContentIos,
      this.androidAppDownloadLink,
      this.directAppDownloadLink,
      this.domain,
      this.fbAccessToken,
      this.fbApiKey,
      this.fbApiSecret,
      this.fbPageId,
      this.fbPageName,
      this.fbPageUrl,
      this.fbUserName,
      this.fcmServerKey,
      this.instagramAccessToken,
      this.instagramPageUrl,
      this.instagramPassword,
      this.instagramUserId,
      this.instagramUserName,
      this.iosAppDownloadLink,
      this.key,
      this.lastAssignedCommentsChannelno,
      this.promotionalBanners,
      this.pubnubPublishKey,
      this.pubnubSubcribeKey,
      this.sessionTimeout,
      this.socialBucketId,
      this.socketUrl,
      this.twitterConsumerKey,
      this.twitterConsumerKeySecret,
      this.twitterOauthAccessToken,
      this.twitterOauthAccessTokenSecret,
      this.twitterPageUrl,
      this.twitterUserName,
      this.androidForceUpdate,
      this.iosForceUpdate,
      this.agoraId,
      this.shoutoutHowToVideoStatus,
      this.defaultAgeRestriction,
      this.logo,
      this.holidayMode,
      this.lastUpdatedGenres,
      this.androidUpdateMessage,
      this.iosUpdateMessage,
      this.minimumAndroidVersionNo,
      this.minimumIosVersionNo,
      this.removePrivateVideoCallIntroVideo,
      this.removeShoutoutPoweredByPhoto,
      this.giftChannelName,
      this.commentChannelName,
      this.artist});

  Artistconfig.fromJson(Map<String, dynamic> json) {
    sId = json['_id'];
    artistId = json['artist_id'];
    channelNamespace = json['channel_namespace'];
    fmcDefaultTopicId = json['fmc_default_topic_id'];
    fmcDefaultTopicIdTest = json['fmc_default_topic_id_test'];
    androidVersionName = json['android_version_name'];
    androidVersionNo = json['android_version_no'];
    iosVersionName = json['ios_version_name'];
    iosVersionNo = json['ios_version_no'];
    lastUpdatedGifts = json['last_updated_gifts'];
    lastUpdatedBuckets = json['last_updated_buckets'];
    lastUpdatedPackages = json['last_updated_packages'];
    lastVisited = json['last_visited'];
    updatedAt = json['updated_at'];
    createdAt = json['created_at'];
    s18PlusAgeContentAndroid = json['18_plus_age_content_android'];
    s18PlusAgeContentIos = json['18_plus_age_content_ios'];
    androidAppDownloadLink = json['android_app_download_link'];
    directAppDownloadLink = json['direct_app_download_link'];
    domain = json['domain'];
    fbAccessToken = json['fb_access_token'];
    fbApiKey = json['fb_api_key'];
    fbApiSecret = json['fb_api_secret'];
    fbPageId = json['fb_page_id'];
    fbPageName = json['fb_page_name'];
    fbPageUrl = json['fb_page_url'];
    fbUserName = json['fb_user_name'];
    fcmServerKey = json['fcm_server_key'];
    instagramAccessToken = json['instagram_access_token'];
    instagramPageUrl = json['instagram_page_url'];
    instagramPassword = json['instagram_password'];
    instagramUserId = json['instagram_user_id'];
    instagramUserName = json['instagram_user_name'];
    iosAppDownloadLink = json['ios_app_download_link'];
    key = json['key'];
    lastAssignedCommentsChannelno = json['last_assigned_comments_channelno'];
    promotionalBanners = json['promotional_banners'];
    pubnubPublishKey = json['pubnub_publish_key'];
    pubnubSubcribeKey = json['pubnub_subcribe_key'];
    sessionTimeout = json['session_timeout'];
    socialBucketId = json['social_bucket_id'];
    socketUrl = json['socket_url'];
    twitterConsumerKey = json['twitter_consumer_key'];
    twitterConsumerKeySecret = json['twitter_consumer_key_secret'];
    twitterOauthAccessToken = json['twitter_oauth_access_token'];
    twitterOauthAccessTokenSecret = json['twitter_oauth_access_token_secret'];
    twitterPageUrl = json['twitter_page_url'];
    twitterUserName = json['twitter_user_name'];
    androidForceUpdate = json['android_force_update'];
    iosForceUpdate = json['ios_force_update'];
    agoraId = json['agora_id'];

    shoutoutHowToVideoStatus = json['shoutout_how_to_video_status'];

    defaultAgeRestriction = json['default_age_restriction'];
    logo = json['logo'] != null ? new Media.fromJson(json['logo']) : null;
    holidayMode = json['holiday_mode'];

    lastUpdatedGenres = json['last_updated_genres'];
    androidUpdateMessage = json['android_update_message'];
    iosUpdateMessage = json['ios_update_message'];
    minimumAndroidVersionNo = json['minimum_android_version_no'];
    minimumIosVersionNo = json['minimum_ios_version_no'];

    removePrivateVideoCallIntroVideo =
        json['remove_private_video_call_intro_video'];

    removeShoutoutPoweredByPhoto = json['remove_shoutout_powered_by_photo'];

    if (json['gift_channel_name'] != null) {
      giftChannelName = json['gift_channel_name'].cast<String>();
    }

    if (json['gift_channel_name'] != null) {
      commentChannelName = json['comment_channel_name'].cast<String>();
    }

    artist =
        json['artist'] != null ? new Artist.fromJson(json['artist']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['_id'] = this.sId;
    data['artist_id'] = this.artistId;
    data['channel_namespace'] = this.channelNamespace;
    data['fmc_default_topic_id'] = this.fmcDefaultTopicId;
    data['fmc_default_topic_id_test'] = this.fmcDefaultTopicIdTest;
    data['android_version_name'] = this.androidVersionName;
    data['android_version_no'] = this.androidVersionNo;
    data['ios_version_name'] = this.iosVersionName;
    data['ios_version_no'] = this.iosVersionNo;
    data['last_updated_gifts'] = this.lastUpdatedGifts;
    data['last_updated_buckets'] = this.lastUpdatedBuckets;
    data['last_updated_packages'] = this.lastUpdatedPackages;
    data['last_visited'] = this.lastVisited;
    data['updated_at'] = this.updatedAt;
    data['created_at'] = this.createdAt;
    data['18_plus_age_content_android'] = this.s18PlusAgeContentAndroid;
    data['18_plus_age_content_ios'] = this.s18PlusAgeContentIos;
    data['android_app_download_link'] = this.androidAppDownloadLink;
    data['direct_app_download_link'] = this.directAppDownloadLink;
    data['domain'] = this.domain;
    data['fb_access_token'] = this.fbAccessToken;
    data['fb_api_key'] = this.fbApiKey;
    data['fb_api_secret'] = this.fbApiSecret;
    data['fb_page_id'] = this.fbPageId;
    data['fb_page_name'] = this.fbPageName;
    data['fb_page_url'] = this.fbPageUrl;
    data['fb_user_name'] = this.fbUserName;
    data['fcm_server_key'] = this.fcmServerKey;
    data['instagram_access_token'] = this.instagramAccessToken;
    data['instagram_page_url'] = this.instagramPageUrl;
    data['instagram_password'] = this.instagramPassword;
    data['instagram_user_id'] = this.instagramUserId;
    data['instagram_user_name'] = this.instagramUserName;
    data['ios_app_download_link'] = this.iosAppDownloadLink;
    data['key'] = this.key;
    data['last_assigned_comments_channelno'] =
        this.lastAssignedCommentsChannelno;
    data['promotional_banners'] = this.promotionalBanners;
    data['pubnub_publish_key'] = this.pubnubPublishKey;
    data['pubnub_subcribe_key'] = this.pubnubSubcribeKey;
    data['session_timeout'] = this.sessionTimeout;
    data['social_bucket_id'] = this.socialBucketId;
    data['socket_url'] = this.socketUrl;
    data['twitter_consumer_key'] = this.twitterConsumerKey;
    data['twitter_consumer_key_secret'] = this.twitterConsumerKeySecret;
    data['twitter_oauth_access_token'] = this.twitterOauthAccessToken;
    data['twitter_oauth_access_token_secret'] =
        this.twitterOauthAccessTokenSecret;
    data['twitter_page_url'] = this.twitterPageUrl;
    data['twitter_user_name'] = this.twitterUserName;
    data['android_force_update'] = this.androidForceUpdate;
    data['ios_force_update'] = this.iosForceUpdate;
    data['agora_id'] = this.agoraId;

    data['shoutout_how_to_video_status'] = this.shoutoutHowToVideoStatus;

    data['default_age_restriction'] = this.defaultAgeRestriction;
    if (this.logo != null) {
      data['logo'] = this.logo.toJson();
    }

    data['last_updated_genres'] = this.lastUpdatedGenres;
    data['android_update_message'] = this.androidUpdateMessage;
    data['ios_update_message'] = this.iosUpdateMessage;
    data['minimum_android_version_no'] = this.minimumAndroidVersionNo;
    data['minimum_ios_version_no'] = this.minimumIosVersionNo;

    data['remove_private_video_call_intro_video'] =
        this.removePrivateVideoCallIntroVideo;

    data['remove_shoutout_powered_by_photo'] =
        this.removeShoutoutPoweredByPhoto;

    data['gift_channel_name'] = this.giftChannelName;
    data['comment_channel_name'] = this.commentChannelName;
    if (this.artist != null) {
      data['artist'] = this.artist.toJson();
    }
    return data;
  }
}
