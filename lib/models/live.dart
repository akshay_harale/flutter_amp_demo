class Live {
  String appId;
  String apiSecret;

  Live({this.appId, this.apiSecret});

  Live.fromJson(Map<String, dynamic> json) {
    appId = json['app_id'];
    apiSecret = json['api_secret'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['app_id'] = this.appId;
    data['api_secret'] = this.apiSecret;
    return data;
  }
}
