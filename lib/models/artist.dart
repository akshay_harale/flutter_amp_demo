import 'package:anveshiapm/models/media.dart';

class Artist {
  String firstName;
  String lastName;
  String slug;
  String lastVisited;
  Media photo;
  String picture;
  String humanReadableCreatedDate;
  String dateDiffForHuman;

  Artist(
      {this.firstName,
      this.lastName,
      this.slug,
      this.lastVisited,
      this.photo,
      this.picture,
      this.humanReadableCreatedDate,
      this.dateDiffForHuman});

  Artist.fromJson(Map<String, dynamic> json) {
    firstName = json['first_name'];
    lastName = json['last_name'];
    slug = json['slug'];
    lastVisited = json['last_visited'];
    photo = json['photo'] != null ? new Media.fromJson(json['photo']) : null;
    picture = json['picture'];
    humanReadableCreatedDate = json['human_readable_created_date'];
    dateDiffForHuman = json['date_diff_for_human'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['first_name'] = this.firstName;
    data['last_name'] = this.lastName;
    data['slug'] = this.slug;
    data['last_visited'] = this.lastVisited;
    if (this.photo != null) {
      data['photo'] = this.photo.toJson();
    }
    data['picture'] = this.picture;
    data['human_readable_created_date'] = this.humanReadableCreatedDate;
    data['date_diff_for_human'] = this.dateDiffForHuman;
    return data;
  }
}
