class Content {
  String id;
  double height;
  String imageUrl;
  String artistId;
  String name;
  String description;
  int coins;
  int likeCount;
  int commentCount;
  String type;
  int ageRestriction;
  String videoUrl;
  String videoThumb;
  int views;
  String duration;
  int durationMs;

  Content({
    this.id,
    this.height,
    this.imageUrl,
    this.artistId,
    this.name,
    this.description,
    this.coins,
    this.likeCount,
    this.ageRestriction,
    this.commentCount,
    this.type,
    this.videoUrl,
    this.duration,
    this.durationMs,
    this.videoThumb,
    this.views,
  });
}
