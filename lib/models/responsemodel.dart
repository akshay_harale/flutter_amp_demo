import 'package:anveshiapm/models/user.detail.dart';

class ResponseModel {
  Data data;
  String message;
  bool error;
  List<String> errorMessage;
  int statusCode;

  ResponseModel(
      {this.data,
      this.message,
      this.error,
      this.errorMessage,
      this.statusCode});

  ResponseModel.fromJson(Map<String, dynamic> json) {
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
    message = json['message'];
    errorMessage = json['error_messages'] != null
        ? List<String>.from(json['error_messages'])
        : null;
    error = json['error'];
    statusCode = json['status_code'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    data['errorMessage'] = this.errorMessage;
    data['message'] = this.message;
    data['error'] = this.error;
    data['status_code'] = this.statusCode;
    return data;
  }
}
