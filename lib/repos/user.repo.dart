import '../appconstant.dart';

import '../models/responsemodel.dart';

import 'package:http/http.dart' as http;
import 'dart:convert';

class UserRepo {
  Map<String, String> headerWithoutAuth = {
    'v': '1.0.0',
    'ApiKey': AppConstant.API_KEY,
    'ArtistId': AppConstant.ARTIST_ID,
    'Platform': AppConstant.PLATFORM
  };

  Future<ResponseModel> tryLoginUser(String email, String password) async {
    try {
      String url =
          '${AppConstant.BASE_URL}customers/auth/login?v=${AppConstant.VERSION}';
      var response = await http.post(url, body: {
        'email': email,
        'password': password,
        'identity': 'email',
        'device_id': 'sdf2387sdgdf',
        'segment_id': '',
        'fcm_id': '',
        'platform': AppConstant.PLATFORM,
        'v': '1.0.0',
      }, headers: {
        'v': '1.0.0',
        'ApiKey': AppConstant.API_KEY,
        'ArtistId': AppConstant.ARTIST_ID,
        'Platform': AppConstant.PLATFORM
      });

      var responseJson = json.decode(response.body);

      if (response.statusCode == 200) {
        var userDetails = ResponseModel.fromJson(responseJson);
        print(json.encode(userDetails.toJson()));
        return userDetails;
      } else {
        if (responseJson['error_messages'] != null) {
          return ResponseModel(
              error: true,
              errorMessage:
                  new List<String>.from(responseJson['error_messages']));
        } else {
          List<String> errorList = ['Something went wrong'];

          return ResponseModel(error: true, errorMessage: errorList);
        }
      }
    } catch (e) {
      print(e.toString());
      List<String> errorMessage = ['User Login Failed'];
      return ResponseModel(error: true, errorMessage: errorMessage);
    }
  }

  Future<ResponseModel> getArtistConfigApi() async {
    try {
      String url =
          '${AppConstant.BASE_URL}customers/artistconfig?v=${AppConstant.VERSION}&artist_id=${AppConstant.ARTIST_ID}&platform${AppConstant.PLATFORM}';

      var response = await http.get(url, headers: headerWithoutAuth);

      var responseJson = json.decode(response.body);

      if (response.statusCode == 200) {
        var artistConfig = ResponseModel.fromJson(responseJson);
        print(json.encode(artistConfig.toJson()));
        return artistConfig;
      } else {
        if (responseJson['error_messages'] != null) {
          return ResponseModel(
              error: true,
              errorMessage:
                  new List<String>.from(responseJson['error_messages']));
        } else {
          List<String> errorList = ['Something went wrong'];

          return ResponseModel(error: true, errorMessage: errorList);
        }
      }
    } catch (e) {
      print(e.toString());
      List<String> errorMessage = ['Something went wrong'];
      return ResponseModel(error: true, errorMessage: errorMessage);
    }
  }

  Future<ResponseModel> getBucketList() async {
    try {
      String url =
          '${AppConstant.BASE_URL}buckets/lists?v=${AppConstant.VERSION}&artist_id=${AppConstant.ARTIST_ID}&platform${AppConstant.PLATFORM}';

      var response = await http.get(url, headers: headerWithoutAuth);

      var responseJson = json.decode(response.body);

      if (response.statusCode == 200) {
        var artistConfig = ResponseModel.fromJson(responseJson);
        print(json.encode(artistConfig.toJson()));
        return artistConfig;
      } else {
        if (responseJson['error_messages'] != null) {
          return ResponseModel(
              error: true,
              errorMessage:
                  new List<String>.from(responseJson['error_messages']));
        } else {
          List<String> errorList = ['Something went wrong'];

          return ResponseModel(error: true, errorMessage: errorList);
        }
      }
    } catch (e) {
      print(e.toString());
      List<String> errorMessage = ['Something went wrong'];
      return ResponseModel(error: true, errorMessage: errorMessage);
    }
  }
}
