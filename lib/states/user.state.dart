import '../models/responsemodel.dart';
import 'package:equatable/equatable.dart';

abstract class UserState extends Equatable {
  @override
  List<Object> get props => [];
}

class InitialState extends UserState {
  InitialState();
  @override
  List<Object> get props => [];
}

class ValidatingUser extends UserState {
  ValidatingUser();
  @override
  List<Object> get props => [];
}

class UserNotLoggedIn extends UserState {
  UserNotLoggedIn();
  @override
  List<Object> get props => [];
}

class ValidUser extends UserState {
  ValidUser();
  @override
  List<Object> get props => [];
}

class LoginResult extends UserState {
  final ResponseModel userDetails;
  LoginResult(this.userDetails);
  @override
  List<Object> get props => [userDetails];
}

class UserLoadErrorState extends UserState {
  final String message;
  UserLoadErrorState(this.message);
  @override
  List<Object> get props => [message];
}

class NoInternetConnection extends UserState {
  NoInternetConnection();
  @override
  List<Object> get props => [];
}

class FetchingData extends UserState {
  FetchingData();
  @override
  List<Object> get props => [];
}

class SuccesApiResult extends UserState {
  final ResponseModel artistConfig;
  final ResponseModel bucketList;
  SuccesApiResult(this.artistConfig, this.bucketList);
  @override
  List<Object> get props => [artistConfig, bucketList];
}

class ErrorState extends UserState {
  final int statusCode;
  final String errorMessage;
  ErrorState(this.statusCode, this.errorMessage);
  @override
  List<Object> get props => [];
}
