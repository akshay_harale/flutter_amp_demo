import 'package:anveshiapm/customthemedata.dart';
import 'package:flutter/material.dart';

class CustomAppDrawer extends StatelessWidget {
  List<String> bucketList = [
    'HOME',
    'VIDEOS',
    'CELEBYTE',
    'VIDEO CALL',
    'DIRECT LINE',
    'PHOTOS',
  ];

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Container(
        decoration: BoxDecoration(gradient: CustomThemeData.drawerGradient),
        child: SingleChildScrollView(
          padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 30),
          child: Column(
            children: <Widget>[
              Row(
                children: <Widget>[
                  CircleAvatar(
                    radius: 20,
                    backgroundImage:
                        AssetImage('assets/images/ic_app_icon.png'),
                  ),
                  Expanded(
                    flex: 5,
                    child: Container(
                      margin: EdgeInsets.only(left: 10, right: 5),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            'VIJAY',
                            softWrap: false,
                            style: TextStyle(
                                fontSize: 14,
                                color: CustomThemeData.primaryTextColor),
                          ),
                          Text(
                            'vijay.singh@armsprime.com',
                            softWrap: false,
                            style: TextStyle(
                                fontSize: 8,
                                color: CustomThemeData.primaryTextColor),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Expanded(
                    flex: 1,
                    child: Icon(
                      Icons.chevron_right,
                      color: CustomThemeData.primaryTextColor,
                      size: 30,
                    ),
                  ),
                ],
              ),
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Container(
                        alignment: Alignment.centerLeft,
                        padding: EdgeInsets.only(top: 20, left: 10, bottom: 10),
                        child: Text(
                          'Account Balance',
                          style: TextStyle(
                              color: CustomThemeData.primaryTextColor,
                              fontSize: 12),
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.only(left: 10, bottom: 10),
                        alignment: Alignment.centerLeft,
                        child: RichText(
                          text: TextSpan(
                            children: [
                              WidgetSpan(
                                  child: Container(
                                alignment: Alignment.centerLeft,
                                padding: EdgeInsets.only(right: 8),
                                child: Image.asset(
                                    'assets/images/ic_arms_coin.png',
                                    height: 18,
                                    width: 18),
                              )),
                              TextSpan(text: '1992')
                            ],
                          ),
                        ),
                      )
                    ],
                  ),
                  Spacer(),
                  Icon(
                    Icons.chevron_right,
                    color: CustomThemeData.primaryTextColor,
                    size: 30,
                  ),
                ],
              ),
              ListView.builder(
                physics: NeverScrollableScrollPhysics(),
                primary: false,
                shrinkWrap: true,
                itemBuilder: (context, index) {
                  return Container(
                    padding: EdgeInsets.all(10),
                    child: Text(
                      bucketList[index],
                      style: TextStyle(
                          fontSize: 13,
                          color: CustomThemeData.drawerItemTextColor),
                    ),
                  );
                },
                itemCount: bucketList.length,
              ),
              Container(
                alignment: Alignment.center,
                height: 80,
                padding: EdgeInsets.only(top: 10, left: 10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Row(
                          children: <Widget>[
                            CircleAvatar(
                              radius: 15,
                              backgroundImage:
                                  AssetImage('assets/images/ic_app_icon.png'),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(left: 8),
                              child: Text(
                                '2 hours ago',
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    color: CustomThemeData.primaryTextColor,
                                    fontSize: 10),
                              ),
                            )
                          ],
                        ),
                        Padding(
                          padding: const EdgeInsets.only(top: 8),
                          child: Text('App Version : 1.0.1',
                              style: TextStyle(
                                  color: CustomThemeData.primaryTextColor,
                                  fontSize: 10)),
                        ),
                      ],
                    ),
                    Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text('Powered By',
                            style: TextStyle(
                                color: CustomThemeData.primaryTextColor,
                                fontSize: 10)),
                        Container(
                            width: 100,
                            height: 40,
                            child: Image.asset(
                              'assets/images/logo_arms.png',
                              fit: BoxFit.contain,
                            )),
                      ],
                    )
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
