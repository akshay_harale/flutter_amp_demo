import 'dart:ui';

import 'package:anveshiapm/appconstant.dart';
import 'package:anveshiapm/models/media.dart';
import 'package:anveshiapm/shared.repo.dart';
import 'package:anveshiapm/widget/photoviewer.dart';

import '../models/list.dart';

import '../customthemedata.dart';
import 'package:flutter/material.dart';

class ContentItem extends StatelessWidget {
  final MediaInfo content;

  ContentItem(this.content);

  double _getContentHeight(Media media) {
    if (media == null) {
      return 100; // default
    } else if (media.smallHeight != null) {
      return media.smallHeight.toDouble();
    } else if (media.coverHeight != null) {
      return media.coverHeight.toDouble();
    } else if (media.mediumHeight != null) {
      return media.mediumHeight.toDouble();
    } else if (media.thumbHeight != null) {
      return media.thumbHeight.toDouble();
    } else {
      return 100;
    }
  }

  @override
  Widget build(BuildContext context) {
    var isVideo = (AppConstant.MEDIA_VIDEO == content.type);
    return Padding(
      padding: const EdgeInsets.all(5.0),
      child: Card(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Row(
              children: <Widget>[
                CircleAvatar(
                  radius: 20,
                  child: Image.asset('assets/images/ic_app_icon.png'),
                  backgroundColor: Colors.transparent,
                ),
                const SizedBox(width: 10),
                const Text('Anveshi  Jain')
              ],
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 5, vertical: 10),
              child: Text(
                content.name,
                style: TextStyle(fontSize: 12),
              ),
            ),
            Stack(
              alignment: Alignment.center,
              children: <Widget>[
                GestureDetector(
                  onTap: () {
                    if (!isVideo) {
                      Navigator.of(context)
                          .pushNamed(PhotoViewer.routeName, arguments: content);
                    } else {
                      Scaffold.of(context).showSnackBar(SnackBar(
                          content: Text('Video player IMPL is pending bro..')));
                    }
                  },
                  child:
                      LoadFreeImageWidget(_getContentHeight, content, isVideo),
                ),
                if (content.coins != null &&
                    content.coins.isNotEmpty &&
                    int.parse(content.coins) != 0)
                  FutureBuilder<bool>(
                    builder: (context, snapshot) {
                      return snapshot.connectionState == ConnectionState.waiting
                          ? Center(
                              child: CircularProgressIndicator(
                                  backgroundColor:
                                      CustomThemeData.progressBarColor))
                          : GestureDetector(
                              onTap: () {
                                Scaffold.of(context).showSnackBar(SnackBar(
                                    content:
                                        Text('Paid logic impl pending..')));
                              },
                              child: snapshot.data
                                  ? LoadFreeImageWidget(
                                      _getContentHeight, content, isVideo)
                                  : LoadPaidImageWidget(
                                      _getContentHeight, content, isVideo),
                            );
                    },
                    future: SharedRepo().isContentPaid(),
                  ),
              ],
            ),
            Container(
              padding: const EdgeInsets.symmetric(vertical: 2, horizontal: 5),
              width: double.infinity,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  FlatButton.icon(
                    onPressed: () {},
                    icon: const Icon(
                      Icons.favorite,
                      color: Colors.red,
                    ),
                    label: Text(
                      '56 Likes',
                      style: Theme.of(context).textTheme.headline4,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 5),
                    child: Text('2 Comments',
                        style: Theme.of(context).textTheme.headline4),
                  ),
                ],
              ),
            ),
            Container(
              height: 1,
              width: double.infinity,
              color: Colors.grey,
              margin: const EdgeInsets.symmetric(horizontal: 10),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                FlatButton.icon(
                  onPressed: () {},
                  icon: const Icon(
                    Icons.favorite,
                    color: Colors.grey,
                  ),
                  label: Text(
                    'Like',
                    style: Theme.of(context).textTheme.headline4,
                  ),
                ),
                FlatButton.icon(
                  onPressed: () {},
                  icon: const Icon(
                    Icons.comment,
                    color: Colors.grey,
                  ),
                  label: Text(
                    'Comment',
                    style: Theme.of(context).textTheme.headline4,
                  ),
                ),
                FlatButton.icon(
                  onPressed: () {},
                  icon: const Icon(
                    Icons.share,
                    color: Colors.grey,
                  ),
                  label: Text(
                    'Share',
                    style: Theme.of(context).textTheme.headline4,
                  ),
                ),
              ],
            ),
            const SizedBox(height: 10)
          ],
        ),
      ),
    );
  }
}

class LoadFreeImageWidget extends StatelessWidget {
  final Function getContentHeight;
  final MediaInfo content;
  final bool isVideo;

  LoadFreeImageWidget(this.getContentHeight, this.content, this.isVideo);
  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: getContentHeight(isVideo ? content.video : content.photo),
      child: Image.network(isVideo ? content.video.small : content.photo.small),
    );
  }
}

class LoadPaidImageWidget extends StatelessWidget {
  final Function getContentHeight;
  final MediaInfo content;
  final bool isVideo;

  LoadPaidImageWidget(this.getContentHeight, this.content, this.isVideo);

  @override
  Widget build(BuildContext context) {
    return ClipRRect(
      child: Container(
        width: double.infinity,
        height: getContentHeight(isVideo ? content.video : content.photo),
        color: Colors.white.withOpacity(0.2),
        child: BackdropFilter(
          filter: ImageFilter.blur(sigmaX: 15, sigmaY: 15),
          child: Container(
            height: isVideo
                ? getContentHeight(content.video)
                : getContentHeight(content.photo),
            alignment: Alignment.center,
            padding: const EdgeInsets.all(10),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Icon(Icons.lock, size: 30, color: CustomThemeData.accentColor),
                Text(
                  'Tap to unlock',
                  softWrap: true,
                  style: Theme.of(context).textTheme.bodyText1,
                ),
                RaisedButton(
                  onPressed: () {},
                  child: Text('Unlock'),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
