import 'dart:io';

import '../appconstant.dart';
import '../widget/contentitems.dart';

import '../models/list.dart';
import '../models/responsemodel.dart';

import 'package:flutter/material.dart';
import '../customthemedata.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class ContentList extends StatefulWidget {
  final String bucketId;
  ContentList(this.bucketId);

  @override
  _ContentListState createState() => _ContentListState();
}

class _ContentListState extends State<ContentList>
    with AutomaticKeepAliveClientMixin {
  var currentPage = 1;
  var perPage = 10;
  var isInternetAvailbale = true;
  var isLoading = false;

  List<MediaInfo> contentList = [];
  ScrollController _scrollController;

  void getFirstPage() async {
    try {
      final internetResult = await InternetAddress.lookup('google.com');
      if (internetResult.isNotEmpty &&
          internetResult[0].rawAddress.isNotEmpty) {
        isLoading = true;
        currentPage = 1;
        contentList.clear();
        print('PAGE : $currentPage');

        String url =
            '${AppConstant.BASE_URL}contents/lists?artist_id=${AppConstant.ARTIST_ID}&platform=${AppConstant.PLATFORM}&bucket_id=${widget.bucketId}&page=$currentPage&perpage=$perPage&v=${AppConstant.VERSION}';

        print(url);
        final response = await http.get(url, headers: {
          'v': AppConstant.VERSION,
          'ApiKey': AppConstant.API_KEY,
          'ArtistId': AppConstant.ARTIST_ID,
          'Platform': AppConstant.PLATFORM
        });

        final responseJson = json.decode(response.body);

        if (response.statusCode == 200) {
          var responseModel = ResponseModel.fromJson(responseJson);
          if (responseModel.data != null &&
              responseModel.data.list != null &&
              responseModel.data.list.isNotEmpty) {
            setState(() {
              contentList.addAll(responseModel.data.list);
              isInternetAvailbale = true;
              currentPage++;
              isLoading = false;
            });
          } else {
            setState(() {
              isInternetAvailbale = true;
              isLoading = false;
            });
          }
        } else {
          setState(() {
            isInternetAvailbale = true;
            isLoading = false;
          });
        }
      } else {
        setState(() {
          isLoading = false;
          isInternetAvailbale = false;
        });
      }
    } catch (e) {
      print(e.toString());
      setState(() {
        isLoading = false;
        isInternetAvailbale = true;
      });
    }
  }

  void getNextPage() async {
    try {
      print('PAGE : $currentPage');
      final internetResult = await InternetAddress.lookup('google.com');
      if (internetResult.isNotEmpty &&
          internetResult[0].rawAddress.isNotEmpty) {
        isLoading = true;

        String url =
            '${AppConstant.BASE_URL}contents/lists?artist_id=${AppConstant.ARTIST_ID}&platform=${AppConstant.PLATFORM}&bucket_id=${widget.bucketId}&page=$currentPage&perpage=$perPage&v=${AppConstant.VERSION}';
        final response = await http.get(url, headers: {
          'v': AppConstant.VERSION,
          'ApiKey': AppConstant.API_KEY,
          'ArtistId': AppConstant.ARTIST_ID,
          'Platform': AppConstant.PLATFORM
        });

        final responseJson = json.decode(response.body);

        if (response.statusCode == 200) {
          var responseModel = ResponseModel.fromJson(responseJson);
          if (responseModel.data != null &&
              responseModel.data.list != null &&
              responseModel.data.list.isNotEmpty) {
            setState(() {
              contentList.addAll(responseModel.data.list);
              isInternetAvailbale = true;
              currentPage++;
              isLoading = false;
            });
          } else {
            setState(() {
              isInternetAvailbale = true;
              isLoading = false;
            });
          }
        } else {
          setState(() {
            isInternetAvailbale = true;
            isLoading = false;
          });
        }
      } else {
        setState(() {
          isLoading = false;
          isInternetAvailbale = false;
        });
      }
    } catch (e) {
      print(e.toString());
      setState(() {
        isLoading = false;
        isInternetAvailbale = true;
      });
    }
  }

  // List<Content> _items = [
  //   Content(
  //     id: '5f2d766c6338904ce07f62b3',
  //     height: 245,
  //     type: 'video',
  //     description:
  //         'Find me where the wild things are 🔥💋 (6 August 2020 LIVE)',
  //     coins: 99,
  //     duration: '41:24',
  //     durationMs: 2484000,
  //     videoUrl:
  //         'https://apagora.s3.ap-south-1.amazonaws.com/cr/5f2bf6ba633890119e6231a8/53343bf91341f105450a3e809b20cf23_anveshijain.m3u8',
  //     likeCount: 228,
  //     commentCount: 30,
  //     name: 'Anveshi Jain',
  //     ageRestriction: 18,
  //     artistId: '5cda8e156338905d962b9472',
  //     videoThumb:
  //         'https://d3m1vhnnekwbrq.cloudfront.net/contents/xsmall-1596814982.jpg',
  //   ),
  // ];

  void _scrollListener() {
    if (_scrollController.offset >=
            _scrollController.position.maxScrollExtent &&
        !_scrollController.position.outOfRange) {
      // "reach the bottom"
      getNextPage();
    }
    if (_scrollController.offset <=
            _scrollController.position.minScrollExtent &&
        !_scrollController.position.outOfRange) {
      // "reach the top"
    }
  }

  @override
  void initState() {
    super.initState();
    getFirstPage();
    print('initcalled');

    _scrollController = ScrollController();
    _scrollController.addListener(_scrollListener);
  }

  @override
  bool get wantKeepAlive => true;

  @override
  void dispose() {
    _scrollController.removeListener(_scrollListener);
    _scrollController.dispose();
    super.dispose();
  }

  @override
  void deactivate() {
    print('implement deactivate');
    super.deactivate();
  }

  @override
  void updateKeepAlive() {
    print('implement updateKeepAlive');
    super.updateKeepAlive();
  }

  @override
  void didChangeDependencies() {
    print('implement didChangeDependencies');
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    print('build called');
    return Container(
      decoration: BoxDecoration(gradient: CustomThemeData.homeGradient),
      child: (contentList.length == 0 && isLoading && isInternetAvailbale)
          ? Center(child: CircularProgressIndicator())
          : (contentList.length == 0 && !isInternetAvailbale)
              ? Center(child: Text('No Internet Connectionn'))
              : ListView.builder(
                  controller: _scrollController,
                  itemCount: contentList.length,
                  itemBuilder: (context, index) {
                    return ContentItem(contentList[index]);
                  },
                ),
    );
  }
}
