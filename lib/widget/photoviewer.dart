import 'package:anveshiapm/models/list.dart';

import 'package:flutter/material.dart';
import 'package:photo_view/photo_view.dart';

class PhotoViewer extends StatelessWidget {
  static const routeName = '/photo_viewer';

  @override
  Widget build(BuildContext context) {
    final MediaInfo mediaInfo = ModalRoute.of(context).settings.arguments;
    return Scaffold(
      body: PhotoView(
        imageProvider: NetworkImage(mediaInfo.photo.cover),
      ),
    );
  }
}
