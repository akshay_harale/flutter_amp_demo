import 'package:flutter/material.dart';

class CelebyteScreen extends StatelessWidget {
  final String bucketId, bucketName, bucketCode;
  CelebyteScreen({this.bucketCode, this.bucketId, this.bucketName});
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Text('Name : $bucketName && Code : $bucketCode'),
    );
  }
}
