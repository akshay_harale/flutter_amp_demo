class AppConstant {
  static const String BASE_URL = 'https://apistg.apmedia.app/api/1.0/';
  static const String API_KEY = 'eynpaQcc7nihvcYuZOuU0TeP7tlNC6o5';
  static const String ARTIST_ID = '5cda8e156338905d962b9472';
  static const String PLATFORM = 'android';
  static const String VERSION = '1.0.0';
  static const String PREF_TOKEN = 'token';

  // Content Type
  static const String MEDIA_PHOTO = 'photo';
  static const String MEDIA_ALBUM = 'album';
  static const String MEDIA_VIDEO = 'video';
}
