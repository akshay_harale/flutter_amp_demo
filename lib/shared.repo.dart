import 'package:anveshiapm/models/artist.config.dart';
import 'package:anveshiapm/models/list.dart';
import 'package:anveshiapm/models/user.detail.dart';

import 'dart:convert';

import 'package:shared_preferences/shared_preferences.dart';

class SharedRepo {
  static SharedPreferences _sharedPreferences;

  static const String PREF_USER_DETAILS = 'user_details';
  static const String PREF_AUTH_TOKEN = 'user_auth_token';
  static const String PREF_ARTIST_CONFIG = 'artist_config';
  static const String PREF_BUCKET_LIST = 'bucket_list';
  static const String PREF_LAST_BUCKET_LIST_UPDATED =
      'last_bucket_list_updated_on';

  Future<SharedPreferences> get instance async {
    if (_sharedPreferences == null) {
      _sharedPreferences = await SharedPreferences.getInstance();
    }
    return _sharedPreferences;
  }

  Future<void> saveCustomerData(CustomerData customerData) async {
    var prefInstance = await instance;
    prefInstance.setString(PREF_USER_DETAILS, customerData.toJson().toString());
  }

  Future<void> saveArtistConfig(Artistconfig artistconfig) async {
    var prefInstance = await instance;
    prefInstance.setString(
        PREF_ARTIST_CONFIG, artistconfig.toJson().toString());
  }

  Future<bool> isBucketListRequiredUpdate(String lastUpdatedOnServer) async {
    var prefInstance = await instance;
    if (prefInstance.containsKey(PREF_LAST_BUCKET_LIST_UPDATED)) {
      var lastUpdatedOnLocal =
          prefInstance.getString(PREF_LAST_BUCKET_LIST_UPDATED);
      if (lastUpdatedOnLocal != null && lastUpdatedOnLocal.isNotEmpty) {
        return lastUpdatedOnServer == lastUpdatedOnLocal;
      } else {
        return false;
      }
    } else {
      return false;
    }
  }

  Future<void> saveBucketsList(
      List<MediaInfo> bucketList, String updateOn) async {
    var prefInstance = await instance;
    var str = bucketList.map((v) => v.toJson()).toList().toString();
    await prefInstance.setString(PREF_BUCKET_LIST, str);
    await prefInstance.setString(PREF_LAST_BUCKET_LIST_UPDATED, updateOn);
  }

  Future<List<MediaInfo>> getBucketList() async {
    var prefInstance = await instance;

    if (prefInstance.containsKey(PREF_BUCKET_LIST)) {
      final bucketList = prefInstance.getString(PREF_BUCKET_LIST);
      if (bucketList != null && bucketList.isNotEmpty) {
        var list = new List<MediaInfo>();
        final jsonBucketList = json.decode(bucketList);
        jsonBucketList.forEach((v) => {list.add(new MediaInfo.fromJson(v))});
        print(jsonBucketList[0]);
        return list;
      } else {
        return new List<MediaInfo>();
      }
    } else {
      return new List<MediaInfo>();
    }
  }

  Future<void> saveToken(String token) async {
    var prefInstance = await instance;
    prefInstance.setString(PREF_AUTH_TOKEN, token);
  }

  Future<bool> isUserLoggedIn() async {
    var prefInstance = await instance;

    if (prefInstance.containsKey(PREF_AUTH_TOKEN)) {
      final token = prefInstance.getString(PREF_AUTH_TOKEN);

      return token != null && token.isNotEmpty;
    } else {
      return false;
    }
  }

  Future<bool> isContentPaid() async {
    return false;
  }
}
