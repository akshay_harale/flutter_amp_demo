import 'package:anveshiapm/blocs/user.bloc.dart';
import 'package:anveshiapm/events/user.events.dart';
import 'package:anveshiapm/models/list.dart';
import 'package:anveshiapm/states/user.state.dart';
import 'package:anveshiapm/widget/celebytescreen.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../customthemedata.dart';
import '../widget/contentlist.dart';
import 'package:flutter/material.dart';
import '../widget/customappdrawer.dart';

class HomeScreen extends StatefulWidget {
  static final String routeName = '/home-screen';

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  void initState() {
    super.initState();
    this.loadArtistConfigAndBucketList();
  }

  void loadArtistConfigAndBucketList() {
    BlocProvider.of<UserBloc>(context).add(GetArtistConfig());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(
            'Home Screen',
            style: Theme.of(context).textTheme.bodyText1,
          ),
          actions: <Widget>[
            Container(
              height: 40,
              width: 40,
              child: ClipRRect(
                borderRadius: BorderRadius.circular(20),
                child: Image.asset('assets/images/ic_app_icon.png'),
              ),
            )
          ],
          centerTitle: true,
          flexibleSpace: Container(
            decoration: BoxDecoration(gradient: CustomThemeData.homeGradient),
          ),
        ),
        drawer: CustomAppDrawer(),
        body: BlocBuilder<UserBloc, UserState>(
          builder: (ctx, state) {
            if (state is ErrorState) {
              return Center(child: Text(state.errorMessage));
            } else if (state is NoInternetConnection) {
              return Center(child: Text('Check your internet connection'));
            } else if (state is SuccesApiResult) {
              // if error is true then it will throw errorstate, here will only handle success case
              return CustomTabsWidget(state.bucketList.data.list);
            } else {
              return Center(
                child: CircularProgressIndicator(
                    backgroundColor: CustomThemeData.fbColor),
              );
            }
          },
        ));
  }
}

class BottomTabView extends StatelessWidget {
  final IconData _iconData;
  final String _label;
  BottomTabView(this._label, this._iconData);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 70,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          Icon(
            _iconData,
            color: Colors.white,
            size: 25,
          ),
          Text(
            _label,
            style: TextStyle(fontSize: 10, color: Colors.white),
          )
        ],
      ),
    );
  }
}

class CustomTabsWidget extends StatefulWidget {
  final List<MediaInfo> bucketList;
  CustomTabsWidget(this.bucketList);

  @override
  _CustomTabsWidgetState createState() => _CustomTabsWidgetState();
}

class _CustomTabsWidgetState extends State<CustomTabsWidget>
    with SingleTickerProviderStateMixin {
  TabController tabController;

  @override
  void initState() {
    super.initState();
    tabController =
        TabController(length: widget.bucketList.length, vsync: this);
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Flexible(
          child: TabBarView(
            children: List.generate(
                widget.bucketList.length,
                (index) => true == widget.bucketList[index].contentMenu
                    ? ContentList(widget.bucketList[index].bucketId)
                    : CelebyteScreen(
                        bucketCode: widget.bucketList[index].code,
                        bucketId: widget.bucketList[index].bucketId,
                        bucketName: widget.bucketList[index].name,
                      )),
            controller: tabController,
          ),
        ),
        Container(
          color: CustomThemeData.primaryColor,
          height: 70,
          width: double.infinity,
          child: TabBar(
            isScrollable: true,
            labelColor: CustomThemeData.primaryTextColor,
            indicatorColor: Colors.white,
            indicatorSize: TabBarIndicatorSize.tab,
            indicatorPadding: EdgeInsets.all(2),
            tabs: List.generate(
                widget.bucketList.length,
                (index) => BottomTabView(
                    widget.bucketList[index].name.toUpperCase(),
                    Icons.video_call)),
            controller: tabController,
          ),
        )
      ],
    );
  }
}
