import 'package:anveshiapm/blocs/user.bloc.dart';
import 'package:anveshiapm/events/user.events.dart';
import 'package:anveshiapm/screens/homescreen.dart';
import 'package:anveshiapm/states/user.state.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../customthemedata.dart';

class LoginWithEmail extends StatefulWidget {
  static const String routeName = '/login-with-email';
  @override
  _LoginWithEmailState createState() => _LoginWithEmailState();
}

class _LoginWithEmailState extends State<LoginWithEmail> {
  bool _obscureText = true;
  bool _rememberMe = false;

  void _togglePassword() {
    setState(() {
      _obscureText = !_obscureText;
    });
  }

  var _emailController = TextEditingController();
  var _passwordController = TextEditingController();

  @override
  void dispose() {
    _emailController.dispose();
    _passwordController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Stack(
          children: <Widget>[
            Container(
              child: Image.asset(
                'assets/images/menu_bg.webp',
                fit: BoxFit.cover,
              ),
              width: double.infinity,
              height: double.infinity,
            ),
            Container(
              decoration: BoxDecoration(
                  gradient: LinearGradient(
                      begin: Alignment.topCenter,
                      end: Alignment.bottomCenter,
                      colors: <Color>[
                    Colors.transparent,
                    CustomThemeData.primaryColor.withOpacity(0.4),
                    CustomThemeData.primaryColor.withOpacity(0.9),
                    CustomThemeData.primaryColorDark
                  ])),
            ),
            SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(
                        top: 20, left: 10, right: 10, bottom: 10),
                    child: const Icon(
                      Icons.chevron_left,
                      color: Colors.white,
                      size: 35,
                    ),
                  ),
                  const SizedBox(height: 60),
                  Container(
                    padding: const EdgeInsets.only(left: 40, right: 40),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          'LOG IN',
                          style: const TextStyle(
                              fontWeight: FontWeight.w300,
                              fontSize: 24,
                              color: Colors.white),
                        ),
                        const SizedBox(height: 30),
                        TextFormField(
                          style: const TextStyle(color: Colors.white),
                          controller: _emailController,
                          cursorColor: Colors.white,
                          textInputAction: TextInputAction.next,
                          keyboardType: TextInputType.text,
                          maxLines: 1,
                          decoration: InputDecoration(
                            labelStyle: const TextStyle(color: Colors.white70),
                            labelText: 'Email Id',
                            focusedBorder: const UnderlineInputBorder(
                              borderSide: BorderSide(color: Colors.white),
                            ),
                            enabledBorder: const UnderlineInputBorder(
                              borderSide: BorderSide(color: Colors.white),
                            ),
                          ),
                        ),
                        const SizedBox(height: 15),
                        TextFormField(
                          style: const TextStyle(color: Colors.white),
                          controller: _passwordController,
                          cursorColor: Colors.white,
                          textInputAction: TextInputAction.done,
                          keyboardType: TextInputType.visiblePassword,
                          maxLines: 1,
                          decoration: InputDecoration(
                            suffixIcon: IconButton(
                                icon: Icon(
                                  _obscureText
                                      ? Icons.visibility
                                      : Icons.visibility_off,
                                  color: Colors.white,
                                ),
                                onPressed: _togglePassword),
                            labelStyle: const TextStyle(color: Colors.white70),
                            labelText: 'Password',
                            focusedBorder: const UnderlineInputBorder(
                              borderSide: BorderSide(color: Colors.white),
                            ),
                            enabledBorder: const UnderlineInputBorder(
                              borderSide: BorderSide(color: Colors.white),
                            ),
                          ),
                          obscureText: _obscureText,
                        ),
                        Padding(
                            padding: const EdgeInsets.only(top: 10),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Row(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: <Widget>[
                                    Checkbox(
                                      checkColor: Colors.amber,
                                      value: _rememberMe,
                                      onChanged: (value) {
                                        setState(() {
                                          _rememberMe = value;
                                        });
                                      },
                                    ),
                                    Text(
                                      'Remember me',
                                      style:
                                          Theme.of(context).textTheme.headline6,
                                    )
                                  ],
                                ),
                                Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Text(
                                    'Forgot Password',
                                    style:
                                        Theme.of(context).textTheme.headline6,
                                  ),
                                )
                              ],
                            ))
                      ],
                    ),
                  ),
                  BlocListener<UserBloc, UserState>(
                    listener: (ctx, state) {
                      if (state is LoginResult) {
                        var message = '';
                        if (state.userDetails.error) {
                          message = state.userDetails.errorMessage[0];
                        } else {
                          message = state.userDetails.message;
                        }
                        Scaffold.of(ctx)
                            .showSnackBar(SnackBar(content: Text(message)));

                        if (!state.userDetails.error) {
                          Navigator.of(context).pushNamed(HomeScreen.routeName);
                        }
                      }
                    },
                    child: BlocBuilder<UserBloc, UserState>(
                      builder: (ctx, state) {
                        return GestureDetector(
                          onTap: () {
                            if (_emailController.text.isEmpty) {
                              return;
                            }
                            if (_passwordController.text.isEmpty) {
                              return;
                            }
                            BlocProvider.of<UserBloc>(context).add(
                                TryLoginEvents(_emailController.text,
                                    _passwordController.text));
                          },
                          child: (state is FetchingData)
                              ? Container(
                                  alignment: Alignment.center,
                                  padding: EdgeInsets.all(10),
                                  child: CircularProgressIndicator(
                                      backgroundColor:
                                          CustomThemeData.primaryColorDark),
                                )
                              : Container(
                                  margin: const EdgeInsets.only(
                                      top: 30, left: 40, right: 40, bottom: 30),
                                  width: double.infinity,
                                  height: 50,
                                  alignment: Alignment.center,
                                  decoration: BoxDecoration(
                                    color: Colors.white,
                                    borderRadius: BorderRadius.circular(10),
                                  ),
                                  child: Text('LOGIN'),
                                ),
                        );
                      },
                    ),
                  ),
                  Container(
                    alignment: Alignment.center,
                    child: Text(
                      'OR',
                      textAlign: TextAlign.center,
                      style: Theme.of(context).textTheme.bodyText1,
                    ),
                  ),
                  Container(
                    margin: const EdgeInsets.only(
                        top: 30, left: 40, right: 40, bottom: 30),
                    width: double.infinity,
                    height: 50,
                    alignment: Alignment.center,
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(10),
                    ),
                    child: Text('GET AN OTP ON YOUR EMAIL'),
                  ),
                  Container(
                    alignment: Alignment.center,
                    padding: const EdgeInsets.only(top: 20, bottom: 50),
                    child: RichText(
                      text: TextSpan(
                          style: TextStyle(
                              color: CustomThemeData.loginGrey, fontSize: 14),
                          children: <TextSpan>[
                            TextSpan(text: 'DONT HAVE AN ACCOUNT? '),
                            TextSpan(
                                text: 'SIGN UP',
                                style: TextStyle(color: Colors.red),
                                recognizer:
                                    TapGestureRecognizer(debugOwner: () {}))
                          ]),
                    ),
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
