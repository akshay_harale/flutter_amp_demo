import 'package:anveshiapm/screens/loginwithemail.dart';

import '../customthemedata.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';

class LoginScreen extends StatefulWidget {
  static final String routeName = '/login';

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }

  @override
  void didUpdateWidget(LoginScreen oldWidget) {
    // TODO: implement didUpdateWidget
    super.didUpdateWidget(oldWidget);
  }

  @override
  void deactivate() {
    // TODO: implement deactivate
    super.deactivate();
  }

  final double indentValue = 5;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        alignment: Alignment.center,
        children: <Widget>[
          Container(
            child: Image.asset(
              'assets/images/menu_bg.webp',
              fit: BoxFit.cover,
            ),
            width: double.infinity,
            height: double.infinity,
          ),
          Container(
            decoration: BoxDecoration(
                gradient: LinearGradient(
                    begin: Alignment.topCenter,
                    end: Alignment.bottomCenter,
                    colors: <Color>[
                  Colors.transparent,
                  CustomThemeData.primaryColor.withOpacity(0.4),
                  CustomThemeData.primaryColor.withOpacity(0.9),
                  CustomThemeData.primaryColorDark,
                ])),
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
              Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(5),
                  color: CustomThemeData.fbColor,
                ),
                margin: const EdgeInsets.all(10),
                width: 300,
                height: 50,
                child: Row(
                  children: <Widget>[
                    Padding(padding: const EdgeInsets.only(left: 8)),
                    Icon(
                      Icons.mail,
                      size: 30,
                      color: Colors.white,
                    ),
                    Padding(padding: const EdgeInsets.only(left: 5)),
                    VerticalDivider(
                      endIndent: indentValue,
                      indent: indentValue,
                      color: Colors.white,
                      thickness: 1,
                    ),
                    Padding(
                      padding: EdgeInsets.only(left: 10),
                      child: Text(
                        'LOGIN WITH FACEBOOK',
                        style: Theme.of(context).textTheme.bodyText1,
                      ),
                    )
                  ],
                ),
              ),
              GestureDetector(
                onTap: () {},
                child: Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5),
                    color: CustomThemeData.gPlusColor,
                  ),
                  margin: EdgeInsets.all(10),
                  width: 300,
                  height: 50,
                  child: Row(
                    children: <Widget>[
                      Padding(padding: EdgeInsets.only(left: 8)),
                      Icon(
                        Icons.mail,
                        size: 30,
                        color: Colors.white,
                      ),
                      Padding(padding: EdgeInsets.only(left: 5)),
                      VerticalDivider(
                        endIndent: indentValue,
                        indent: indentValue,
                        color: Colors.white,
                        thickness: 1,
                      ),
                      Padding(
                        padding: EdgeInsets.only(left: 10),
                        child: Text(
                          'LOGIN WITH GMAIL',
                          style: Theme.of(context).textTheme.bodyText1,
                        ),
                      )
                    ],
                  ),
                ),
              ),
              GestureDetector(
                onTap: () {
                  Navigator.of(context).pushNamed(LoginWithEmail.routeName);
                },
                child: Container(
                  decoration: BoxDecoration(
                    border: Border.all(color: Colors.white),
                    borderRadius: BorderRadius.circular(5),
                    color: Colors.transparent,
                  ),
                  margin: EdgeInsets.all(10),
                  width: 300,
                  height: 50,
                  child: Row(
                    children: <Widget>[
                      Padding(padding: EdgeInsets.only(left: 8)),
                      Icon(
                        Icons.mail,
                        size: 30,
                        color: Colors.white,
                      ),
                      Padding(padding: EdgeInsets.only(left: 5)),
                      VerticalDivider(
                        endIndent: indentValue,
                        indent: indentValue,
                        color: Colors.white,
                        thickness: 1,
                      ),
                      Padding(
                        padding: EdgeInsets.only(left: 10),
                        child: Text(
                          'LOGIN WITH EMAIL',
                          style: Theme.of(context).textTheme.bodyText1,
                        ),
                      )
                    ],
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 20, bottom: 50),
                child: RichText(
                  text: TextSpan(
                      style: TextStyle(
                          color: CustomThemeData.loginGrey, fontSize: 14),
                      children: <TextSpan>[
                        TextSpan(text: 'DONT HAVE AN ACCOUNT? '),
                        TextSpan(
                            text: 'SIGN UP',
                            style: TextStyle(color: Colors.red),
                            recognizer: TapGestureRecognizer(debugOwner: () {}))
                      ]),
                ),
              ),
              Text('Why Login?',
                  style: TextStyle(
                      fontSize: 18,
                      color: Colors.white,
                      fontWeight: FontWeight.bold)),
              Padding(
                padding: const EdgeInsets.all(5),
                child: Table(
                  children: <TableRow>[
                    TableRow(children: <Widget>[
                      Padding(
                        padding:
                            const EdgeInsets.only(left: 10, top: 5, bottom: 10),
                        child: Text(
                          '• Get Free Coins',
                          style: Theme.of(context).textTheme.headline4,
                        ),
                      ),
                      Padding(
                        padding:
                            const EdgeInsets.only(left: 10, top: 5, bottom: 10),
                        child: Text(
                          '• Watch Exclusive Content',
                          style: Theme.of(context).textTheme.headline4,
                        ),
                      ),
                    ]),
                    TableRow(children: <Widget>[
                      Padding(
                        padding:
                            const EdgeInsets.only(left: 10, top: 5, bottom: 5),
                        child: Text(
                          '• Attend Live Sessions',
                          style: Theme.of(context).textTheme.headline4,
                        ),
                      ),
                      Padding(
                        padding:
                            const EdgeInsets.only(left: 10, top: 5, bottom: 5),
                        child: Text(
                          '• Directly Message The Artist',
                          style: Theme.of(context).textTheme.headline4,
                        ),
                      ),
                    ])
                  ],
                ),
              ),
              SizedBox(
                height: 30,
              ),
            ],
          )
        ],
      ),
    );
  }
}
