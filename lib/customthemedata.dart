import 'package:flutter/material.dart';

class CustomThemeData {
  static final Color primaryColor = Color(0xFF6A001A);
  static final Color accentColor = Color(0xFFF7F6F6);
  static final Color primaryColorDark = Color(0xFF3F0C00);
  static final Color primaryColorLight = Color(0xFFB10028);
  static final Color primaryExtraDark = Color(0xFF480009);
  static final Color fbColor = Color(0xFF3B5998);
  static final Color gPlusColor = Color(0xFFb23121);
  static final Color loginGrey = Color(0xFFababab);
  static final Color progressBarColor = Color(0xFF6A001A);
  // Text Colors
  static final Color primaryTextColor = Color(0xFFFFFFFF);
  static final Color secondaryTextColor = Color(0xFF000000);
  static final Color drawerItemTextColor = Color(0xFFff919c);

  static final Gradient homeGradient = LinearGradient(
    begin: Alignment.topCenter,
    end: Alignment.bottomCenter,
    colors: <Color>[
      primaryExtraDark,
      primaryColorDark,
      primaryColor,
      primaryColorLight,
    ],
  );

  static final Gradient drawerGradient = LinearGradient(
    begin: Alignment.topCenter,
    end: Alignment.bottomCenter,
    colors: <Color>[
      secondaryTextColor,
      primaryExtraDark,
    ],
  );
}
