import 'dart:io';
import '../shared.repo.dart';
import '../repos/user.repo.dart';
import '../events/user.events.dart';
import '../states/user.state.dart';
import 'package:bloc/bloc.dart';

class UserBloc extends Bloc<UserEvents, UserState> {
  UserBloc(UserState initialState) : super(initialState);

  @override
  Stream<UserState> mapEventToState(UserEvents event) async* {
    yield InitialState();
    if (event is TryLoginEvents) {
      try {
        final internetResult = await InternetAddress.lookup('google.com');
        if (internetResult.isNotEmpty &&
            internetResult[0].rawAddress.isNotEmpty) {
          print('connected');

          UserRepo userRepo = UserRepo();

          yield FetchingData();
          final result =
              await userRepo.tryLoginUser(event.userName, event.password);

          if (!result.error) {
            await SharedRepo().saveToken(result.data.token);

            await SharedRepo().saveCustomerData(result.data.customer);
          }
          yield LoginResult(result);
        } else {
          print('not connected');
          yield NoInternetConnection();
        }
      } on SocketException catch (_) {
        print('not connected');
        yield NoInternetConnection();
      }
    } else if (event is GetArtistConfig) {
      try {
        final internetResult = await InternetAddress.lookup('google.com');
        if (internetResult.isNotEmpty &&
            internetResult[0].rawAddress.isNotEmpty) {
          print('connected');

          yield FetchingData();

          UserRepo userRepo = UserRepo();

          final artistConfig = await userRepo.getArtistConfigApi();

          if (artistConfig.error) {
            yield ErrorState(
                artistConfig.statusCode, artistConfig.errorMessage[0]);
          } else {
            await SharedRepo().saveArtistConfig(artistConfig.data.artistconfig);

            final bucketList = await userRepo.getBucketList();

            if (bucketList.error) {
              yield ErrorState(
                  bucketList.statusCode, bucketList.errorMessage[0]);
            } else {
              await SharedRepo().saveBucketsList(bucketList.data.list,
                  artistConfig.data.artistconfig.lastUpdatedBuckets);
              yield SuccesApiResult(artistConfig, bucketList);
            }

// Logic to avoid bucket list call everytime if not updated.
            // ResponseModel bucketList;
            // var updateRequired = await SharedRepo().isBucketListRequiredUpdate(
            //     artistConfig.data.artistconfig.lastUpdatedBuckets);
            // if (updateRequired) {

            // } else {
            //   bucketList = ResponseModel();
            //   bucketList.data.list = await SharedRepo().getBucketList();
            //   yield SuccesApiResult(artistConfig, bucketList);
            // }
          }
        } else {
          print('not connected');
          yield NoInternetConnection();
        }
      } on SocketException catch (_) {
        print('not connected');
        yield NoInternetConnection();
      }
    } else if (event is AuthenticateUser) {
      yield ValidatingUser();

      final shareRepo = SharedRepo();

      final auth = await shareRepo.isUserLoggedIn();

      if (auth) {
        yield ValidUser();
      } else {
        yield UserNotLoggedIn();
      }
    }
  }
}
