import 'package:anveshiapm/blocs/user.bloc.dart';
import 'package:anveshiapm/customthemedata.dart';
import 'package:anveshiapm/screens/login.dart';
import 'package:anveshiapm/screens/loginwithemail.dart';
import 'package:anveshiapm/splash.dart';
import 'package:anveshiapm/states/user.state.dart';
import 'package:anveshiapm/widget/photoviewer.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import './screens/homescreen.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => UserBloc(InitialState()),
      child: MaterialApp(
        title: 'Flutter Demo',
        theme: ThemeData(
          fontFamily: 'Raleway',
          primaryColor: CustomThemeData.primaryColor,
          primaryColorDark: CustomThemeData.primaryColorDark,
          accentColor: CustomThemeData.accentColor,
          primaryColorLight: CustomThemeData.primaryColorLight,
          primarySwatch: Colors.blue,
          visualDensity: VisualDensity.adaptivePlatformDensity,
          buttonTheme: ButtonThemeData(
              shape: new RoundedRectangleBorder(
                borderRadius: new BorderRadius.circular(10.0),
              ),
              buttonColor: CustomThemeData.primaryColorLight,
              textTheme: ButtonTextTheme.accent),
          textTheme: TextTheme(
            headline3: TextStyle(
                fontSize: 12, color: CustomThemeData.secondaryTextColor),
            headline4: TextStyle(
                fontSize: 12, color: CustomThemeData.primaryTextColor),
            button: TextStyle(fontSize: 12),
            bodyText1: TextStyle(
                fontSize: 14,
                fontWeight: FontWeight.normal,
                color: CustomThemeData.primaryTextColor),
            bodyText2: TextStyle(
                fontSize: 14,
                fontWeight: FontWeight.normal,
                color: CustomThemeData.secondaryTextColor),
            headline5: TextStyle(
                fontSize: 10,
                fontWeight: FontWeight.normal,
                color: CustomThemeData.secondaryTextColor),
            headline6: TextStyle(
                fontSize: 10,
                fontWeight: FontWeight.normal,
                color: CustomThemeData.primaryTextColor),
          ),
          accentTextTheme: TextTheme(
            button: TextStyle(
                fontSize: 12,
                fontWeight: FontWeight.normal,
                color: CustomThemeData.primaryTextColor),
            bodyText1: TextStyle(
                fontSize: 14,
                fontWeight: FontWeight.normal,
                color: CustomThemeData.primaryTextColor),
            bodyText2: TextStyle(
                fontSize: 14,
                fontWeight: FontWeight.normal,
                color: CustomThemeData.secondaryTextColor),
          ),
        ),
        home: SplashScreen(),
        routes: {
          LoginScreen.routeName: (ctx) => LoginScreen(),
          HomeScreen.routeName: (ctx) => HomeScreen(),
          LoginWithEmail.routeName: (ctx) => LoginWithEmail(),
          PhotoViewer.routeName: (ctx) => PhotoViewer(),
        },
      ),
    );
  }
}
