import 'package:equatable/equatable.dart';

abstract class UserEvents extends Equatable {
  @override
  List<Object> get props => [];
}

class TryLoginEvents extends UserEvents {
  final String userName, password;
  TryLoginEvents(this.userName, this.password);
  @override
  List<Object> get props => [userName, password];
}

class GetArtistConfig extends UserEvents {
  GetArtistConfig();
  @override
  List<Object> get props => [];
}

class GetBucketList extends UserEvents {
  GetBucketList();
  @override
  List<Object> get props => [];
}

class AuthenticateUser extends UserEvents {
  AuthenticateUser();
  @override
  List<Object> get props => [];
}
